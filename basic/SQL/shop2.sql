-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Авг 22 2016 г., 12:28
-- Версия сервера: 5.5.50
-- Версия PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `shop2`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `parent_id`, `name`, `keywords`, `description`) VALUES
(33, 0, 'Портативная техника', '', ''),
(34, 33, 'Ноутбуки', '', ''),
(35, 33, 'Жесткие диски для ноутбуков', '', ''),
(36, 0, 'Компьютерная техника', 'Компьютерная техника', 'Компьютерная техника'),
(37, 36, 'Компьютеры', 'Компьютеры', 'Компьютеры'),
(38, 36, 'Игровые приставки', 'Игровые приставки', 'Игровые приставки'),
(39, 38, 'Консоли Sony PlayStation', 'Консоли Sony PlayStation', 'Консоли Sony PlayStation'),
(40, 38, 'Консоли Xbox', 'Консоли Xbox', 'Консоли Xbox'),
(41, 0, 'Бытовая техника', 'Бытовая техника', 'Бытовая техника'),
(42, 0, 'Планшеты', 'Планшеты', 'Планшеты'),
(43, 0, 'Мобильные телефоны', 'Мобильные телефоны', 'Мобильные телефоны'),
(44, 0, 'Фото и видеотехника', 'Фото и видеотехника', 'Фото и видеотехника'),
(45, 0, 'Музыкальное оборудование', 'Музыкальное оборудование', 'Музыкальное оборудование'),
(46, 0, 'Спорт и туризм', 'Спорт и туризм', 'Спорт и туризм'),
(47, 41, 'Наушники', 'Наушники', 'Наушники');

-- --------------------------------------------------------

--
-- Структура таблицы `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `id` int(11) NOT NULL,
  `filePath` varchar(400) NOT NULL,
  `itemId` int(20) NOT NULL,
  `isMain` int(1) DEFAULT NULL,
  `modelName` varchar(150) NOT NULL,
  `urlAlias` varchar(400) NOT NULL,
  `name` varchar(80) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `image`
--

INSERT INTO `image` (`id`, `filePath`, `itemId`, `isMain`, `modelName`, `urlAlias`, `name`) VALUES
(5, 'Products/Product1/220eb5.jpg', 1, 1, 'Product', '63edd4e6b6-1', ''),
(6, 'Products/Product1/3ffd03.jpg', 1, NULL, 'Product', '0c35c6c7ac-2', ''),
(7, 'Products/Product4/d3c4fc.jpg', 4, 0, 'Product', '9c8d6ecf68-3', ''),
(8, 'Products/Product4/474ece.jpg', 4, 0, 'Product', 'be8fa0d0f1-4', ''),
(9, 'Products/Product4/ed0711.jpg', 4, 0, 'Product', 'd473789557-5', ''),
(10, 'Products/Product4/cb75b6.jpg', 4, 0, 'Product', '6ede329352-6', ''),
(11, 'Products/Product4/662f21.jpg', 4, 0, 'Product', 'd5a104bef0-2', ''),
(12, 'Products/Product4/54c9d9.jpg', 4, 1, 'Product', 'f6f6bd16c5-1', ''),
(13, 'Products/Product8/9afa8e.jpg', 8, 1, 'Product', '8f4c847809-1', ''),
(14, 'Products/Product8/99cfcf.jpg', 8, NULL, 'Product', 'd3ce7b19ab-2', ''),
(15, 'Products/Product8/9e4b43.jpg', 8, NULL, 'Product', '4649271536-3', ''),
(16, 'Products/Product8/20524c.jpg', 8, NULL, 'Product', 'fe0d71e55f-4', ''),
(17, 'Products/Product8/e7b95b.jpg', 8, NULL, 'Product', '8150455c94-5', ''),
(18, 'Products/Product14/f19dc7.jpg', 14, 0, 'Product', 'f7aea124de-2', ''),
(19, 'Products/Product14/6890ec.jpg', 14, 1, 'Product', '23afad270f-1', ''),
(20, 'Products/Product14/13891b.jpg', 14, NULL, 'Product', '557627b34c-3', ''),
(21, 'Products/Product15/fdcbe8.jpg', 15, 1, 'Product', '1482991927-1', ''),
(22, 'Products/Product15/429e64.jpg', 15, NULL, 'Product', 'd05a76c27a-2', ''),
(23, 'Products/Product15/6ad847.jpg', 15, NULL, 'Product', '2abf17d819-3', ''),
(24, 'Products/Product15/69157d.jpg', 15, NULL, 'Product', '4e54a36dcf-4', ''),
(25, 'Products/Product16/ce85f5.jpg', 16, 1, 'Product', '757eb57554-1', ''),
(26, 'Products/Product16/b86670.jpg', 16, NULL, 'Product', 'ea2b355d57-2', ''),
(27, 'Products/Product16/8476da.jpg', 16, NULL, 'Product', 'd51ec2c3f7-3', ''),
(28, 'Products/Product16/d8effa.jpg', 16, NULL, 'Product', 'ee89e24e0a-4', ''),
(29, 'Products/Product17/53e52c.jpg', 17, 1, 'Product', 'e6762412d1-1', ''),
(30, 'Products/Product17/74f103.jpg', 17, NULL, 'Product', 'ca4c654000-2', ''),
(31, 'Products/Product17/00b054.jpg', 17, NULL, 'Product', '84ca7fac25-3', ''),
(32, 'Products/Product17/76b795.jpg', 17, NULL, 'Product', 'b85409794b-4', ''),
(33, 'Products/Product17/a93d16.jpg', 17, NULL, 'Product', 'd9b74768ab-5', ''),
(34, 'Products/Product19/d61003.jpg', 19, 1, 'Product', '2229945084-1', ''),
(35, 'Products/Product19/b01db4.jpg', 19, NULL, 'Product', 'b5d6f2b3eb-2', ''),
(36, 'Products/Product20/307d31.jpg', 20, 1, 'Product', 'cfa6c6ed82-1', ''),
(37, 'Products/Product21/37ccc0.jpg', 21, 1, 'Product', '731dd6d9fa-1', ''),
(38, 'Products/Product22/ac32f3.jpg', 22, 1, 'Product', '2194d7541d-1', ''),
(39, 'Products/Product22/e8ab53.jpg', 22, NULL, 'Product', 'cc03095c9f-2', ''),
(40, 'Products/Product22/a8dc92.jpg', 22, NULL, 'Product', 'fcaa120563-3', ''),
(41, 'Products/Product23/1d83bf.jpg', 23, 1, 'Product', '5c0752a94a-1', ''),
(42, 'Products/Product23/fdca29.jpg', 23, NULL, 'Product', 'aff6954fa6-2', ''),
(43, 'Products/Product23/ed645e.jpg', 23, NULL, 'Product', 'd3202dee19-3', ''),
(44, 'Products/Product23/6217be.jpg', 23, NULL, 'Product', 'e6f0b555c6-4', ''),
(45, 'Products/Product24/5a1024.jpg', 24, 1, 'Product', '4c1cc24202-1', ''),
(46, 'Products/Product24/cbddad.jpg', 24, NULL, 'Product', 'ebfe71eb41-2', ''),
(47, 'Products/Product24/3d2daf.jpg', 24, NULL, 'Product', 'b38745d47d-3', ''),
(48, 'Products/Product24/d161f2.jpg', 24, NULL, 'Product', '321a0a14ca-4', ''),
(49, 'Products/Product25/200a8c.jpg', 25, 1, 'Product', '55e4934142-1', ''),
(50, 'Products/Product25/ce1bdb.jpg', 25, NULL, 'Product', '3bf81958a3-2', ''),
(51, 'Products/Product26/a9f463.jpg', 26, 1, 'Product', '554523929b-1', ''),
(52, 'Products/Product27/0aa19e.jpg', 27, 1, 'Product', '94e016af2e-1', ''),
(53, 'Products/Product28/d97608.jpg', 28, 1, 'Product', '2cbebea06a-1', ''),
(54, 'Products/Product29/3c69e2.jpg', 29, 1, 'Product', '93c7e127f7-1', ''),
(55, 'Products/Product29/dcd51c.jpg', 29, NULL, 'Product', 'c6d5479711-2', ''),
(56, 'Products/Product29/0616e1.jpg', 29, NULL, 'Product', '9b7acc5418-3', ''),
(57, 'Products/Product29/fe57bb.jpg', 29, NULL, 'Product', 'e5da52cd13-4', ''),
(58, 'Products/Product29/a6f112.jpg', 29, NULL, 'Product', '08d5960bec-5', ''),
(59, 'Products/Product30/f8ff32.jpg', 30, 1, 'Product', '07cec799f4-1', ''),
(60, 'Products/Product31/a9b7cf.jpg', 31, 1, 'Product', 'a53cdfbf61-1', ''),
(61, 'Products/Product32/58be86.jpg', 32, 1, 'Product', '2fdf794a26-1', ''),
(62, 'Products/Product33/100e98.jpg', 33, 1, 'Product', '144e453b2a-1', ''),
(63, 'Products/Product33/1b78fe.jpg', 33, NULL, 'Product', 'd4dc3ce8ca-2', ''),
(64, 'Products/Product33/0c54ea.jpg', 33, NULL, 'Product', 'ab129de8ab-3', ''),
(65, 'Products/Product33/7e96e8.jpg', 33, NULL, 'Product', '267f5a8b03-4', ''),
(66, 'Products/Product33/d29b50.jpg', 33, NULL, 'Product', 'e8960a58f9-5', '');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1471762245),
('m140622_111540_create_image_table', 1471762259),
('m140622_111545_add_name_to_image_table', 1471762259);

-- --------------------------------------------------------

--
-- Структура таблицы `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `qty` int(10) NOT NULL,
  `sum` float NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order`
--

INSERT INTO `order` (`id`, `created_at`, `updated_at`, `qty`, `sum`, `status`, `name`, `email`, `phone`, `address`) VALUES
(3, '2016-08-18 12:22:13', '2016-08-18 12:22:13', 46, 2350, '0', 'sadfsadf', 'shakespeareinlove@yandex.ru', '+79250245709', '47 Petaling street, China town'),
(5, '2016-08-18 12:28:58', '2016-08-18 12:28:58', 46, 2350, '0', 'fghjfghjfghjfgh', 'shakespeareinlove@yandex.ru', '+79250245709', '11111111'),
(6, '2016-08-18 14:26:39', '2016-08-18 14:26:39', 1, 56, '1', 'Петя', 'shakespeareinlove@yandex.ru', '+60320220811', '47 Petaling street, China town'),
(7, '2016-08-18 14:29:23', '2016-08-18 14:29:23', 1, 20, '0', 'fghjfghjfghjfgh', 'shakespeareinlove@yandex.ru', '+79250245709', 'Москва'),
(8, '2016-08-19 10:50:09', '2016-08-19 10:50:09', 2, 56, '0', 'Петя', 'shakespeareinlove@yandex.ru', '+60320220811', 'Москва');

-- --------------------------------------------------------

--
-- Структура таблицы `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `qty_item` int(11) NOT NULL,
  `sum_item` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` text,
  `price` float NOT NULL DEFAULT '0',
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT 'no-image.png',
  `hit` enum('0','1') NOT NULL DEFAULT '0',
  `new` enum('0','1') NOT NULL DEFAULT '0',
  `sale` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `category_id`, `name`, `content`, `price`, `keywords`, `description`, `img`, `hit`, `new`, `sale`) VALUES
(15, 34, 'Ноутбук Lenovo IdeaPad 100-15IBD', '<p>Диагональ</p>\r\n\r\n<p>15.6&quot;</p>\r\n\r\n<p>Процессор</p>\r\n\r\n<p>Intel Core i3-5005U 2.00 GHz</p>\r\n\r\n<p>Жесткий диск</p>\r\n\r\n<p>500 Gb HDD</p>\r\n\r\n<p>Память</p>\r\n\r\n<p>4 Gb DDR3</p>\r\n\r\n<p>Видеокарта</p>\r\n\r\n<p>Intel HD Graphics 5500</p>\r\n\r\n<p>ОС</p>\r\n\r\n<p>DOS</p>\r\n\r\n<p>WiFi</p>\r\n\r\n<p>Да</p>\r\n\r\n<p>Bluetooth</p>\r\n\r\n<p>Да</p>\r\n\r\n<p>Вес</p>\r\n\r\n<p>2.3 кг</p>\r\n', 25490, '', '', 'no-image.png', '1', '0', '0'),
(16, 34, 'Ноутбук Acer Aspire E5-573-39HC NX.MVHER.032', '<h3>Acer Aspire E5-573-39HC Core i3 5005U/4Gb/500Gb/15.6&quot; HD/DVD/Win10 Grey</h3>\r\n', 28390, '', '', 'no-image.png', '0', '0', '0'),
(17, 34, 'Ультрабук UltraBook Asus Zenbook UX303UA 90NB08V1-M04170', '<h3>Asus Zenbook UX303UA Core i3 6100U/6Gb/500Gb/13.3&quot;/Cam/Win10 Smoky brown</h3>\r\n', 53900, '', '', 'no-image.png', '1', '0', '0'),
(18, 33, 'Ультрабук UltraBook Asus Zenbook UX303UA 90NB08V3-M03310', '<h3>Asus Zenbook UX303UA Core i7 6500U/12Gb/256Gb SSD/13.3&quot; FHD/Win10 <span style="color:#FF8C00">Rose Gold</span></h3>\r\n', 82690, '', '', 'no-image.png', '0', '0', '0'),
(19, 35, 'Внутренний жесткий диск 2,5" ST500LT012', '<h3>500Gb 2.5&quot; Seagate (ST500LT012) 16Mb 5400rpm SATA2 Momentus Thin</h3>\r\n', 2990, 'жесткий диск', '', 'no-image.png', '0', '0', '0'),
(20, 33, 'Игровая приставка Sony PS4', '<p>500Gb White (CUH-1108A)</p>\r\n', 27990, 'Sony', '', 'no-image.png', '0', '1', '0'),
(21, 39, 'Игровая приставка Sony PS4', '<p>500Gb White (CUH-1108A)</p>\r\n', 27990, '', '', 'no-image.png', '1', '1', '0'),
(22, 40, 'Игровая приставка Microsoft Xbox 360 E ', '<p>500GB + Forza Horizon 2 + Forza 4</p>\r\n', 10990, '', '', 'no-image.png', '0', '0', '0'),
(23, 34, 'Ноутбук Acer Aspire F5-573G-57K3 NX.GD6ER.002', '<h3>Acer Aspire F5-573G-57K3 Core i5 6200U/6Gb/1Tb/NV GTX950M 4Gb/15.6&quot; FHD/DVD/Win10 <u><strong>Black</strong></u></h3>\r\n', 52170, '', '', 'no-image.png', '0', '0', '0'),
(24, 47, 'Наушники GAL SLR-650', '<p><span style="color:rgb(0, 0, 0); font-family:verdana,arial,sans-serif; font-size:11px">Проводной, разъём 3.5 мм</span></p>\r\n', 375, '', '', 'no-image.png', '0', '1', '0'),
(25, 42, 'Планшет Galaxy Tab 4 SM-T280 silver', '<h3>Samsung Galaxy Tab A 7.0 SM-T280 8Gb <span style="color:#D3D3D3">silver</span></h3>\r\n', 10185, '', '', 'no-image.png', '0', '0', '0'),
(26, 43, 'Смартфон Lenovo IdeaPhone Vibe Z2 Dual Sim Gray', '<p>Вес</p>\r\n\r\n<p>158 гр</p>\r\n\r\n<p>Время работы в режиме ожидания</p>\r\n\r\n<p>до 408 ч</p>\r\n\r\n<p>Bluetooth</p>\r\n\r\n<p>Да</p>\r\n\r\n<p>WiFi</p>\r\n\r\n<p>Да</p>\r\n\r\n<p>3G</p>\r\n\r\n<p>Да</p>\r\n\r\n<p>Поддержка двух SIM-карт</p>\r\n\r\n<p>Да</p>\r\n\r\n<p>Встроенный GPS приемник</p>\r\n\r\n<p>Да</p>\r\n\r\n<p>Операционная система/платформа</p>\r\n\r\n<p>Android 4.4</p>\r\n\r\n<p>Разрешение</p>\r\n\r\n<p>1280 x 720</p>\r\n\r\n<p>Гарантия</p>\r\n\r\n<p>12 мес.</p>\r\n', 18900, '', '', 'no-image.png', '1', '0', '0'),
(27, 43, 'Смартфон ASUS ZenFone 2 ZE550KL 16Gb 90AZ00L1-M00470', '<h3>ASUS ZenFone 2 Laser ZE550KL 16Gb Ram 2Gb LTE 5.5&quot; Dual Sim Black</h3>\r\n\r\n<div class="detail_short_props" style="margin: 0px; padding: 0px; border: 0px; font-size: 11px; vertical-align: baseline; color: rgb(160, 160, 160); line-height: 15px; display: inline-block; font-family: Verdana, Arial, sans-serif;">\r\n<p>Вес</p>\r\n\r\n<p>170 гр</p>\r\n\r\n<p>Bluetooth</p>\r\n\r\n<p>Да</p>\r\n\r\n<p>WiFi</p>\r\n\r\n<p>Да</p>\r\n\r\n<p>3G</p>\r\n\r\n<p>Да</p>\r\n\r\n<p>Поддержка двух SIM-карт</p>\r\n\r\n<p>Да</p>\r\n\r\n<p>Встроенный GPS приемник</p>\r\n\r\n<p>Да</p>\r\n\r\n<p>Операционная система/платформа</p>\r\n\r\n<p>Android 5.0</p>\r\n\r\n<p>Разрешение</p>\r\n\r\n<p>1280 x 720</p>\r\n\r\n<p>Гарантия</p>\r\n\r\n<p>12 мес.</p>\r\n</div>\r\n', 13290, '', '', 'no-image.png', '0', '0', '0'),
(28, 43, 'Смартфон Meizu M2 mini', '<h3>Meizu M2 mini 16Gb Gray</h3>\r\n\r\n<div class="detail_short_props" style="margin: 0px; padding: 0px; border: 0px; font-size: 11px; vertical-align: baseline; color: rgb(160, 160, 160); line-height: 15px; display: inline-block; font-family: Verdana, Arial, sans-serif;">\r\n<h5>Кратко о&nbsp;главном!</h5>\r\n\r\n<ul>\r\n	<li>Поддержка двух&nbsp;SIM-карт&nbsp;в&nbsp;режиме 4G</li>\r\n	<li>Четырехъядерный процессор и&nbsp;2 Гб&nbsp;оперативной памяти</li>\r\n	<li>Качественные камеры</li>\r\n</ul>\r\n\r\n<h5>Эргономичный смартфон с&nbsp;поддержкой&nbsp;2-х&nbsp;SIM-карт</h5>\r\n\r\n<p>Благодаря корпусу из&nbsp;поликарбоната&nbsp;5-дюймовый&nbsp;смартфон Meizu M2 приятно лежит в&nbsp;руке, не&nbsp;выскальзывая и&nbsp;не&nbsp;доставляя никаких неудобств. Также этот аппарат позволяет подключить две&nbsp;SIM-карты, причем обе могут работать в&nbsp;режиме 4G.</p>\r\n\r\n<h5>Легко справится со&nbsp;всеми повседневными задачами</h5>\r\n\r\n<p>Смартфон Meizu M2 mini без проблем справляется со&nbsp;всеми повседневными задачами, включая воспроизведение мультимедийных файлов,&nbsp;веб-серфинг, общение в&nbsp;социальных сетях и&nbsp;так далее. Но&nbsp;это и&nbsp;неудивительно, ведь аппарат снабжен четырехъядерным процессором и&nbsp;оперативной памятью объемом 2 Гб.</p>\r\n\r\n<h5>Отличный подарок для любителей фотосъемки</h5>\r\n\r\n<p>Еще одно достоинство этого смартфона&nbsp;&mdash; основная камера от&nbsp;Samsung, оснащенная качественной матрицей с&nbsp;разрешением 13&nbsp;Мп, которая порадует отличными фотографиями и&nbsp;видеороликами. Также к&nbsp;вашим услугам фронтальная камера с&nbsp;технологией FotoNation 2.0, улучшающей качество селфи.</p>\r\n</div>\r\n', 11070, '', '', 'no-image.png', '0', '0', '0'),
(29, 43, 'Смартфон Meizu M3 Note 16Gb', '<h3>Meizu M3 Note 16Gb Gold/White</h3>\r\n\r\n<div class="detail_short_props" style="margin: 0px; padding: 0px; border: 0px; font-size: 11px; vertical-align: baseline; color: rgb(160, 160, 160); line-height: 15px; display: inline-block; font-family: Verdana, Arial, sans-serif;">\r\n<p>Вес</p>\r\n\r\n<p>163 гр</p>\r\n\r\n<p>Bluetooth</p>\r\n\r\n<p>Да</p>\r\n\r\n<p>WiFi</p>\r\n\r\n<p>Да</p>\r\n\r\n<p>3G</p>\r\n\r\n<p>Да</p>\r\n\r\n<p>Поддержка двух SIM-карт</p>\r\n\r\n<p>Да</p>\r\n\r\n<p>Встроенный GPS приемник</p>\r\n\r\n<p>Да</p>\r\n\r\n<p>Операционная система/платформа</p>\r\n\r\n<p>Android 5.1</p>\r\n\r\n<p>Разрешение</p>\r\n\r\n<p>1920 x 1080</p>\r\n\r\n<p>Гарантия</p>\r\n\r\n<p>12 мес</p>\r\n</div>\r\n', 16990, '', '', 'no-image.png', '0', '0', '0'),
(30, 44, 'Видеокамера Panasonic HC-W570 Black', '<p>Матрица</p>\r\n\r\n<p>MOS 2.51 мегапикс.</p>\r\n\r\n<p>Оптический ZOOM</p>\r\n\r\n<p>50x</p>\r\n', 26790, '', '', 'no-image.png', '1', '0', '0'),
(31, 45, 'Виниловый проигрыватель Ion Pure LP Usb Red', '<p><strong>ION Audio Pure LP</strong>&nbsp;простой в использовании, красивый и качественный проигрыватель, разработан для желающих попробовать, что такое винил и чем он отличается от цифрового звука. Корпус проигрывателя может быть белым, голубым, красным, или под дерево. ION Audio Pure LP имеет функцию оцифровки и подключается по USB к любому компьютеру.</p>\r\n\r\n<ul>\r\n	<li><span style="color:#FF0000">Деревянный корпус с качественным лаковым покрытием</span></li>\r\n	<li><span style="color:#FF0000">Пять цветов корпуса</span></li>\r\n	<li><span style="color:#FF0000">Функция оцифровки</span></li>\r\n	<li><span style="color:#FF0000">Пылезащитная крышка</span></li>\r\n</ul>\r\n', 4990, '', '', 'no-image.png', '1', '0', '1'),
(32, 33, 'Фонарь ЭРА SDB1', '<h3>ЭРА SDB1 1x0.5W LED 1хАА алюминий</h3>\r\n', 170, '', '', 'no-image.png', '1', '0', '0'),
(33, 46, 'Автохолодильник Rolsen RCB-120', '<p>Объём сумки (л)</p>\r\n\r\n<p>20</p>\r\n', 2690, '', '', 'no-image.png', '1', '1', '0');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `auth_key` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `auth_key`) VALUES
(1, 'admin', '$2y$13$BqrEe.NkVXgxFObGAKcY/.M9mq.u4X0wJZt4t6ed/LU6HyJY.oOtS', 'Mku5gCBIA8v6r_GvEDE2iLUjCncHgEsL');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT для таблицы `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT для таблицы `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
