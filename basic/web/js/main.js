/*price range*/

 $('#sl2').slider();
    //включение метода аккордиона для левого меню категорий
    $('.catalog').dcAccordion({
        speed: 300
    });
    
    //Модальное окно корзины
    function showCart(cart){
        //передача данных о добавляемом товаре в корзину в тело модального окна
        $('#cart .modal-body').html(cart);
        //включение модального окна корзины
        $('#cart').modal();
    }
    
    //Открытие корзины при нажатии на значек корзины в header
    function getCart(){
        $.ajax({
            url: '/cart/show',
            type: 'GET',
            success: function(res){
                if(!res){
                    alert('Ошибка!');
                }
                //после успешного выполнения запроса открывается корзина
                showCart(res);
            },
            error: function(){
                alert('Error');
            }
        });
        
        //для отмены перехода по ссылке # возвращается false
        return false;
    }
    
    //Очистка корзины
    function clearCart(){
        $.ajax({
            url: '/cart/clear',
            type: 'GET',
            success: function(res){
                if(!res){
                    alert('Ошибка!');
                }
                //после успешного выполнения запроса снова открывается корзина
                showCart(res);
            },
            error: function(){
                alert('Error');
            }
        });
    }
    
    //Удаление одного товара из корзины при нажатии на крестик
    $('#cart .modal-body').on('click', '.del-item', function(){
        var id = $(this).data('id');
        $.ajax({
            url: '/cart/del-item',
            data: {id: id},
            type: 'GET',
            success: function(res){
                if(!res){
                    alert('Ошибка!');
                }
                //после успешного выполнения запроса снова открывается корзина
                showCart(res);
            },
            error: function(){
                alert('Error');
            }
        })
    });
    
//Ajax запрос на добавления товара в корзину
$('.add-to-cart').on('click', function(e){
    //отключени обработки нажатия клавиш
    e.preventDefault();
    //получение id товара
    var id = $(this).data('id'),
    //получение количества товара
        qty = $('#qty').val();
    $.ajax({
        url: '/cart/add',
        //передача id и количества товара
        data: {id: id, qty: qty},
        type: 'GET',
        success: function(res){
            //Если товара с таким id в БД возвращаем ошибку
            if(!res){
                alert('Ошибка!');
            }
            //после успешного выполнения запроса открывается корзина
            showCart(res);
        },
        error: function(){
            alert('Error');
        }
    })
});

	var RGBChange = function() {
	  $('#RGB').css('background', 'rgb('+r.getValue()+','+g.getValue()+','+b.getValue()+')')
	};	
		
/*scroll to top*/

$(document).ready(function(){
	$(function () {
		$.scrollUp({
	        scrollName: 'scrollUp', // Element ID
	        scrollDistance: 300, // Distance from top/bottom before showing element (px)
	        scrollFrom: 'top', // 'top' or 'bottom'
	        scrollSpeed: 300, // Speed back to top (ms)
	        easingType: 'linear', // Scroll to top easing (see http://easings.net/)
	        animation: 'fade', // Fade, slide, none
	        animationSpeed: 200, // Animation in speed (ms)
	        scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
					//scrollTarget: false, // Set a custom target element for scrolling to the top
	        scrollText: '<i class="fa fa-angle-up"></i>', // Text for element, can contain HTML
	        scrollTitle: false, // Set a custom <a> title if required.
	        scrollImg: false, // Set true to use image
	        activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
	        zIndex: 2147483647 // Z-Index for the overlay
		});
	});
});
