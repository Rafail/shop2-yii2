<?php

namespace app\controllers;
use app\models\Category;
use app\models\Product;
use Yii;
use yii\web\HttpException;

class ProductController extends AppController {
    
    public function actionView($id){
        //Получение id товара через Get запрос
//        $id = Yii::$app->request->get('id');
        //Ленивая загрузка данных товара по id из БД
        $product =  Product::findOne($id);
        //В случае, если такого товара нет, выводится страница с ошибкой
        if(empty($product)){
            throw new HttpException(404, 'Что-то пошло не так и страница с товаром не была найдена');
        }
        
        //Жадная загрузка
//        $product = Product::find()->with('category')->where(['id' => $id])->limit(1)->one();
        //Запрос к БД для Популярные товары
        $hits = Product::find()->where(['hit' => '1'])->limit(9)->all();
        //Установка метатегов
        $this->setMeta('Тестовый магазин на Yii2 | ' . $product->name, $product->keywords,
                $product->description);
        
        return $this->render('view', compact('product', 'hits'));
    }
}
