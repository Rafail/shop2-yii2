<?php

namespace app\controllers;
use app\models\Product;
use app\models\Cart;
use app\models\Order;
use app\models\OrderItems;
use Yii;

class CartController extends AppController{
    //Добавление товара в корзину Ajax запросом
    public function actionAdd(){
        //получение id товара добавляемого в корзину
        $id = Yii::$app->request->get('id');
        //получение количества товара и приведение к целому числу
        $qty = (int)Yii::$app->request->get('qty');
        //Если qty не число, в корзину добавляется 1 товар,
        // иначе количество qty
        $qty = !$qty ? 1 : $qty;
        
        //запрос к БД
        $product = Product::findOne($id);
        //если запрос ничего не вернул
        if(empty($product)){
            return FALSE;
        }
        //откытие сессии
        $session = Yii::$app->session;
        $session->open();
        
        //модель
        $cart = new Cart();
        $cart->addToCart($product, $qty);
        
        //Добавление в корзину в случае отключения javascript
        if (!Yii::$app->request->isAjax){
            return $this->redirect(Yii::$app->request->referrer);
        }
        
        //отключение шаблона
        $this->layout = FALSE;
        
        return $this->render('cart-modal', compact('session'));
    }
    //Очистка корзины
    public function actionClear(){
        //открытие сессии
        $session = Yii::$app->session;
        $session->open();
        
        //удаление из сессии всех данных о товарах
        $session->remove('cart');
        $session->remove('cart.qty');
        $session->remove('cart.sum');
        
        //отключение шаблона
        $this->layout = FALSE;
        
        return $this->render('cart-modal', compact('session'));
    }
    
    //Удаление одного товара из корзины при нажатии на крестик
    public function actionDelItem(){
        //получение id товара удаляемого из корзины
        $id = Yii::$app->request->get('id');
        
        //открытие сессии
        $session = Yii::$app->session;
        $session->open();
        
        //Подключение модели
        $cart = new Cart();
        $cart->recalc($id);
        
        //отключение шаблона
        $this->layout = FALSE;
        
        return $this->render('cart-modal', compact('session'));
    }
    
    //Открытие корзины при нажатии на значек в Header
    public function actionShow(){
        
        //открытие сессии
        $session = Yii::$app->session;
        $session->open();
        
        //отключение шаблона
        $this->layout = FALSE;
        
        return $this->render('cart-modal', compact('session'));
    }
    //Обработка заказа
    public function actionView(){
        //открытие сессии
        $session = Yii::$app->session;
        $session->open();
        
        //Установка заголовка для страницы
        $this->setMeta('Корзина');
        
        //Подключение модели
        $order = new Order();
        
        if($order->load(Yii::$app->request->post())){
            $order->qty = $session['cart.qty'];
            $order->sum = $session['cart.sum'];
            
            if($order->save()){
                $this->saveOrderItems($session['cart'], $order->id);
                Yii::$app->session->setFlash('success', 'Ваш заказ принят.');
                
                //отправка письма заказа
                Yii::$app->mailer->compose('order', ['session' => $session])
                        ->setFrom(['test@mail.ru' => 'shop2'])
                        ->setTo($order->email)
                        ->setSubject('Заказ')
                        ->send();
                
                //очистка корзины
                $session->remove('cart');
                $session->remove('cart.qty');
                $session->remove('cart.sum');
                        
                return $this->refresh();
            }else{
                Yii::$app->session->setFlash('error', 'Ошибка оформления заказа!');
            }
        }
        
        return $this->render('view', compact('session', 'order'));
    }
    
    //Сохранение заказанных товаров в таблицу order_items
    protected function saveOrderItems($items, $order_id){
        foreach($items as $id => $item){
            $order_items = new OrderItems();
            $order_items->order_id = $order_id;
            $order_items->product_id = $id;
            $order_items->name = $item['name'];
            $order_items->price = $item['price'];
            $order_items->qty_item = $item['qty'];
            $order_items->sum_item = $item['qty'] * $item['price'];
            $order_items->save();
        }
    }
}
