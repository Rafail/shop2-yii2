<?php

namespace app\controllers;
use app\models\Category;
use app\models\Product;
use yii\data\Pagination;
use yii\web\HttpException;
use Yii;

class CategoryController extends AppController {
    
    public function actionIndex(){
        //Запрос к БД для Популярные товары
        $hits = Product::find()->where(['hit' => '1'])->limit(9)->all();
        $this->setMeta('Тестовый магазин на Yii2');
        return $this->render('index', compact('hits'));
    }
    
    public function actionView($id){
//        $id = Yii::$app->request->get('id');
        //Запрос на категорию по id
        $category = Category::findOne($id);
        //В случае, если такой категории нет выводится страница с ошибкой
        if(empty($category)){
            throw new HttpException(404, 'Что-то пошло не так и мы не смогли найти нужную категорию');
        }
        
//        $products = Product::find()->where(['category_id' => $id])->all();
        //Включение пагинации для товаров по категориям
        $query = Product::find()->where(['category_id' => $id]);
        $pages = new Pagination(['totalCount' => $query->count(),
            'pageSize' => 3, 'forcePageParam' => FALSE, 'pageSizeParam' => FALSE]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();
        
        //Установка метатегов
        $this->setMeta('Тестовый магазин на Yii2 | ' . $category->name, $category->keywords,
                $category->description);
        
        return $this->render('view', compact('products', 'pages', 'category'));
    }
    
    public function actionSearch(){
        //получение поискового запроса с учетом пробелов
        $q = trim(Yii::$app->request->get('q'));
        
        //Установка метатегов
        $this->setMeta('Тестовый магазин на Yii2 | Поиск: ' . $q);
        
        //обработка поиска в случае, если строка поиска пустая
        if(!$q){
            return $this->render('search');
        }
        //Включение пагинации для товаров по категориям из поискового запроса
        $query = Product::find()->where(['like', 'name', $q]);
        
        $pages = new Pagination(['totalCount' => $query->count(),
            'pageSize' => 3, 'forcePageParam' => FALSE, 'pageSizeParam' => FALSE]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();
        
        return $this->render('search', compact('products', 'pages', 'q'));
    }
}
