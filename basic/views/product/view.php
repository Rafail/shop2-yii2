<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Категории</h2>
						<ul class="catalog category-products"><!--Левое меню категорий-->
                                                <?= \app\components\MenuWidget::Widget(['tpl' => 'menu']); ?>
                                                </ul><!--/Левое меню категорий-->
					</div>
				</div>
<?php 
$mainImage = $product->getImage(); 
$gallery = $product->getImages();
?>
				<div class="col-sm-9 padding-right">
					<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
<!--								<img src="/images/product-details/1.jpg" alt="" />-->
                                                                <?= Html::img($mainImage->getUrl(), ['alt' => $product->name]) ?>
<!--								<h3>ZOOM</h3>-->
							</div>
                                                    
							<div id="similar-product" class="carousel slide" data-ride="carousel">
                                                            <div class="carousel-inner"><!--Wrapper for slides -->
                                                                <?php $count = count($gallery); $i = 0; foreach ($gallery as $img): ?>
                                                                <?php if($i % 3 == 0): ?>
                                                                <div class="item <?php if ($i == 0){echo ' active';}?>">
                                                                <?php endif; ?>
                                                                      <a href="">
                                                                          <?= Html::img($img->getUrl('84x85'), ['alt' => $product->name]) ?>
                                                                      </a>
                                                                <?php $i++; if($i % 3 == 0 || $i == $count): ?>
                                                                    </div>
                                                                <?php endif; ?>
                                                                <?php endforeach; ?>
                                                            </div><!--Wrapper for slides -->

                                                                            <!--Controls -->
								  <a class="left item-control" href="#similar-product" data-slide="prev">
									<i class="fa fa-angle-left"></i>
								  </a>
								  <a class="right item-control" href="#similar-product" data-slide="next">
									<i class="fa fa-angle-right"></i>
								  </a>
							</div>

						</div>
						<div class="col-sm-7">
							<div class="product-information"><!--/product-information-->
<!--								<img src="/images/product-details/new.jpg" class="newarrival" alt="" />-->
                                                                <?php if($product->new): ?>
                                                                    <?= Html::img("@web/images/home/new.png", ['alt' => 'Новинка', 'class' => 'new']); ?>
                                                                    <?php endif; ?>
                                                                    <?php if($product->sale): ?>
                                                                    <?= Html::img("@web/images/home/sale.png", ['alt' => 'Распродажа', 'class' => 'new']); ?>
                                                                <?php endif; ?>
                                                            
								<h2><?= $product->name ?></h2>
								<p>Web ID: 1089772</p>
								<img src="/images/product-details/rating.png" alt="" />
								<span>
									<span>&#8381;<?= $product->price ?></span>
									<label>Количество:</label>
									<input type="text" value="1" id="qty" />
                                                                        
                                                                        <a href="<?= Url::to(['cart/add', 'id' => $product->id]) ?>"
                                                                           data-id="<?= $product->id ?>"
                                                                           class="btn btn-fefault cart add-to-cart">
										<i class="fa fa-shopping-cart"></i>
										В корзину
									</a>
								</span>
<!--								<p><b>Availability:</b> In Stock</p>
								<p><b>Condition:</b> New</p>-->
                                                                <p><b>Категория:</b> <a href="<?= Url::to(['category/view', 'id' => $product->category->id])?>"><?= $product->category->name ?></a></p>
                                                                
                                                                
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
                                        
                                        <div><!--Вывод описания товара-->
                                            <h2>Описание:</h2>
                                             <?= $product->content ?>
               
                                        </div><!--/Вывод описания товара-->
                                        <br>
                                        
					
					<div class="recommended_items"><!--features_items-->
						<h2 class="title text-center">Популярные товары</h2>
						
						<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
                                                            <?php $count = count($hits); $i = 0; foreach ($hits as $hit): ?>
								<?php if($i % 3 == 0): ?>
                                                                <div class="item <?php if($i == 0){echo 'active';} ?>">
                                                                <?php endif; ?>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
                                                                                                    <a href="<?= Url::to(['product/view', 'id' => $hit->id]) ?>">
                                                                                                        <?php $mainImage = $hit->getImage();?>
                                                                                                        <?= Html::img($mainImage->getUrl(), ['alt' => $hit->name]); ?>
                                                                                                    </a>
													<h2>&#8381;<?= $hit->price?></h2>
                                                                                                    <a href="<?= Url::to(['product/view', 'id' => $hit->id]) ?>">
													<p><?= $hit->name ?></p>
                                                                                                    </a>
                                                                                                        <a href="<?= Url::to(['cart/add', 'id' => $hit->id]) ?>"
                                                                                                           data-id="<?= $hit->id ?>"
                                                                                                           class="btn btn-fefault cart add-to-cart">
                                                                                                                <i class="fa fa-shopping-cart"></i>
                                                                                                                В корзину
                                                                                                        </a>
<!--													<button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзину</button>-->
												</div>
											</div>
										</div>
									</div>
                                                                <?php $i++; if($i % 3 == 0 || $i == $count): ?>
								</div>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
							</div>
							 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							  </a>
							  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							  </a>			
						</div>
					</div><!--/features_items-->
					
				</div>
			</div>
		</div>
	</section>

