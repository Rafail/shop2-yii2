<?php

namespace app\models;
use yii\db\ActiveRecord;

class Cart extends ActiveRecord {
    
    public function addToCart($product, $qty = 1){
        $mainImage = $product->getImage();
        //если в сессии корзины уже имеется товар
        if(isset($_SESSION['cart'][$product->id])){
            //увеличивается его количество на 1
            $_SESSION['cart'][$product->id]['qty'] += $qty;
        } else {
            //иначе вся информация о товаре записывается в сессию
            $_SESSION['cart'][$product->id] = [
                'qty' => $qty,
                'name' => $product->name,
                'price' => $product->price,
                'img' => $mainImage->getUrl('x50')
            ];
        }
        //подсчет общего количества товаров в корзине
        //если товар уже есть в корзине, то увеличиваем его количество
        //на величину, которую получили параметром
        $_SESSION['cart.qty'] = isset($_SESSION['cart.qty']) ? $_SESSION['cart.qty']
                        //или начинаем увеличивать его количество
                + $qty : $qty;
        //аналогично для суммы
        $_SESSION['cart.sum'] = isset($_SESSION['cart.sum']) ? $_SESSION['cart.sum']
                + $qty * $product->price : $qty * $product->price;
    }
    
    //метод для удаления товара из корзины по id и пересчета суммы и количества товаров
    public function recalc($id){
        if(!isset($_SESSION['cart'][$id])){
            return FALSE;
        }
        
        $qtyMinus = $_SESSION['cart'][$id]['qty'];
        $sumMinus = $_SESSION['cart'][$id]['qty'] * $_SESSION['cart'][$id]['price'];
        
        $_SESSION['cart.qty'] -= $qtyMinus;
        $_SESSION['cart.sum'] -= $sumMinus;
        
        unset($_SESSION['cart'][$id]);
    }
}
