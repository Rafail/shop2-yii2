<?php

namespace app\components;
use yii\base\Widget;
use app\models\Category;
use Yii;

class MenuWidget extends Widget {
    //шаблон меню
    public $tpl;
    //хранит записи категорий из БД
    public $data;
    //результат работы функции
    public $tree;
    //хранит готовый html в зависимости от шаблона
    public $menuHtml;
    
    public $model;
    
    public function init(){
        parent::init();
        if ( $this->tpl === null){
            $this->tpl = 'menu';
        }
        $this->tpl .= '.php';
    }

        public function run(){
            //Кеширование только для пользовательской части
            if($this->tpl == 'menu.php'){
                //получаем кэш меню категорий
                $menu = Yii::$app->cache->get('menu');
                //если меню есть в кэше, используем его
                if ($menu){
                    return $menu;
                }
            }
            
            $this->data = Category::find()->indexBy('id')->asArray()->all();
            $this->tree = $this->getTree();
            $this->menuHtml = $this->getMenuHtml($this->tree);
            
            //Кеширование только для пользовательской части
            if($this->tpl == 'menu.php'){
                //добавляем меню в кэш
                Yii::$app->cache->set('menu', $this->menuHtml, 60);
            }
            
        return $this->menuHtml;
    }
    //строительство дерева из одномерного массива
    protected function getTree(){
        $tree = [];
        foreach ($this->data as $id=>&$node){
            if (!$node['parent_id']){
                $tree[$id] = &$node;
            } else {
                $this->data[$node['parent_id']]['childs'][$node['id']] = &$node;
            }
        }
        return $tree;
    }
    
    protected function getMenuHtml($tree, $tab = ''){
        $str = '';
        foreach ($tree as $category){
            $str .= $this->catToTemplate($category, $tab);
        }
        return $str;
    }
    
    protected function catToTemplate($category, $tab){
        ob_start();
        include __DIR__ . '/menu_tpl/' . $this->tpl;
        return ob_get_clean();
    }
}
